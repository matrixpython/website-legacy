<div align="center">

  <!-- Keep the empty line between the div and this line on GitLab -->
  <a href="https://matrixpython.gitlab.io/website-legacy">
    <img src="https://gitlab.com/matrixpython/designs/-/raw/main/gitlab_logos/website_legacy_logo/website_legacy_logo_1024.png"
         alt="MPWebsite"
         width="200"
    >
    </img>
  </a>
  <h1>Python Community on Matrix (Legacy) Website</h1>
  <br />

<h4>
  We are the Python Community on Matrix, a free and open network for secure,
  decentralized communication.
</h4>

  https://matrixpython.gitlab.io/website-legacy/

<p>
  <a href="https://gitlab.com/matrixpython/website-legacy/-/blob/main/LICENSE.md">
    <img alt="License"
         src="https://img.shields.io/badge/license-MIT-blue"
    >
    </img>
  </a>
  <a href="https://matrixpython.gitlab.io/">
    <img alt="Website"
         src="https://img.shields.io/website?url=https%3A%2F%2Fmatrixpython.gitlab.io/website-legacy%2F"
    >
    </img>
  </a>
  <a href="https://gitlab.com/matrixpython/website-legacy/-/commits/main">
    <img alt="GitLab last commit"
         src="https://img.shields.io/gitlab/last-commit/matrixpython/website-legacy"
    >
  </a>
</p>

<p>
  <a href="#website-status">Website Status</a> •
  <a href="#contributing">Contributing</a> •
  <a href="#contact">Contact</a> •
  <a href="#license">License</a>
</p>

</div>


## Website Status

This website is still in maintenance mode. No new features will be added.

It is planned to be replaced with our new website, as soon it is released.

## Contributing

If you want to get involved with the development of our new
website, check our [Contributing Wiki Page](https://matrixpython.gitlab.io/wiki/Help:Introduction/). There you will find
all necessary information to get started, how our triage process works and
the documentation.

## Contact

We have prepared an
[FAQ section](https://matrixpython.gitlab.io/docs/help/faq/), which might
answer your questions. If not, simply
[contact us](https://matrixpython.gitlab.io/contact/).

## License

Copyright &copy; 2023 Python Community on Matrix \
Copyright &copy; 2023 Michael Sasser <Info@MichaelSasser.org>

Released under the
[MIT](https://gitlab.com/matrixpython/matrixpython.gitlab.io/blob/master/LICENSE.md)
license.

Based on the [OneDly project](https://github.com/cdeck3r/OneDly-Theme) theme,
built with [Hugo](https://gohugo.io/).

The license of this repositories logo: https://gitlab.com/matrixpython/designs/-/blob/main/gitlab_logos/website_logo
